import java.util.stream.Stream;

class PhoneRed<sum> {

    private String name;
    private int price;

    public PhoneRed(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 6 S", 54000),
            new Phone("Lumia 950", 45000),
            new Phone("Samsung Galaxy S 6", 40000),
            new Phone("LG G 4", 32000));

    int sum = phoneStream.reduce(0,
            (x, y) -> {
                if (y.getPrice() < 50000)
                    return x + y.getPrice();
                else
                    return x + 0;
            },
            (x, y)->x + y);

   // System.out.println(sum);

    }
