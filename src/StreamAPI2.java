import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class StreamAPI2 {
    public static void main(String[] args) {

        ArrayList<String> cities = new ArrayList<String>();
        Collections.addAll(cities, "Париж", "Лондон", "Мадрид");

        Stream<String> citiesStream = cities.stream();
        citiesStream.forEach(s -> System.out.println(s));
        long number = citiesStream.count();
        System.out.println(number);
        citiesStream = citiesStream.filter(s -> s.length() > 5);
    }
}
