import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamAPI6 {
    public static void main(String[] args) {
        Stream<String> citiesStream =Stream.of("Париж", "Лондон", "Мадрид");
        citiesStream.forEach(s->System.out.println(s));

        String[] cities = {"Париж", "Лондон", "Мадрид"};
        Stream<String> citiesStream2 =Stream.of(cities);

        IntStream intStream = IntStream.of(1,2,4,5,7);
        intStream.forEach(i->System.out.println(i));

        LongStream longStream = LongStream.of(100,250,400,5843787,237);
        longStream.forEach(l->System.out.println(l));

        DoubleStream doubleStream = DoubleStream.of(3.4, 6.7, 9.5, 8.2345, 121);
        doubleStream.forEach(d->System.out.println(d));
    }
}
