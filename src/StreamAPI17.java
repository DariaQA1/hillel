import java.util.stream.Stream;
import java.util.Optional;
import java.util.*;
public class StreamAPI17 {

    public static void main(String[] args) {

        ArrayList<String> names = new ArrayList<String>();
        names.addAll(Arrays.asList(new String[]{"Tom", "Sam", "Bob", "Alice"}));
        System.out.println(names.stream().count());

        System.out.println(names.stream().filter(n->n.length()<=3).count());
    }
}