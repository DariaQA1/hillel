class Collect2{

    private String name;
    private int price;

    public Collect2(String name, int price){
        this.name=name;
        this.price=price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

}