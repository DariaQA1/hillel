import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class Optional4 {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        Optional<Integer> min = numbers.stream().min(Integer::compare);
        numbers.addAll(Arrays.asList(new Integer[]{4,5,6,7,8,9}));
        min = numbers.stream().min(Integer::compare);
        System.out.println(min.orElse(-1));
    }
}
