import java.util.ArrayList;
import java.util.Optional;

public class Optional3 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        Optional<Integer> min = numbers.stream().min(Integer::compare);
        System.out.println(min.orElse(-1));

    }
}
