package filter;

import java.util.stream.Stream;

public class Phone3 {
    private String name;
    private int price;

    public Phone3(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public static void main(String[] args) {
        Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 6 S", 54000), new Phone("Lumia 950", 45000),
                new Phone("Samsung Galaxy S 6", 40000));
        phoneStream
                .map(p-> "название: " + p.getName() + " цена: " + p.getPrice())
                .forEach(s -> System.out.println(s));
    }
}
