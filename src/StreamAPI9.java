import java.util.stream.Stream;

public class StreamAPI9 {
    public static void main(String[] args) {
        Stream<String> citiesStream = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        citiesStream.filter(s->s.length()==6).forEach(s->System.out.println(s));
    }
}
