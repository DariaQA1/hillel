import java.util.stream.Stream;
import java.util.Optional;

public class StreamAPI22 {

    public static void main(String[] args) {


        Stream<Integer> numbersStream = Stream.of(1,2,3,4,5,6);
        Optional<Integer> result = numbersStream.reduce((x,y)->x*y);
        System.out.println(result.get()); // 720
    }
}