import java.util.stream.Stream;

public class StreamAPI14 {
    public static void main(String[] args) {
        Stream<String> people = Stream.of("Tom", "Bob", "Sam", "Tom", "Alice", "Kate", "Sam");
        people.distinct().forEach(p -> System.out.println(p));
    }
}
