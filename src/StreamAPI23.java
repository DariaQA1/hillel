import java.util.Optional;
import java.util.stream.Stream;

public class StreamAPI23 {
    public static void main(String[] args) {
        Stream<String> wordsStream = Stream.of("мама", "мыла", "раму");
        Optional<String> sentence = wordsStream.reduce((x, y)->x + " " + y);
        System.out.println(sentence.get());
    }
}
