import java.util.stream.Stream;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Arrays;
public class StreamAPI19 {

    public static void main(String[] args) {

        ArrayList<String> names = new ArrayList<String>();
        names.addAll(Arrays.asList(new String[]{"Tom", "Sam", "Bob", "Alice"}));

        // есть ли в потоке строка, длина которой больше 3
        boolean any = names.stream().anyMatch(s->s.length()>3);
        System.out.println(any);

        boolean all = names.stream().allMatch(s->s.length()==3);
        System.out.println(all);

        boolean none = names.stream().noneMatch(s->s=="Bill");
        System.out.println(none);
    }
}